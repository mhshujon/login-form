<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 8/17/2017
 * Time: 2:22 PM
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
    <main class="container py-5">
        <form action="store.php" method="post" enctype="multipart/form-data">
            <table>

                <tr>
                    <td>Enter your name</td>
                    <td>: <input type="text" name="name"></td>
                </tr>
                <tr>
                    <td>Enter your email</td>
                    <td>: <input type="email" name="email"></td>
                </tr>
                <tr>
                    <td>Enter your password</td>
                    <td>: <input type="password" name="password"></td>
                </tr>
                <tr>
                    <td>Enter your mobile number</td>
                    <td>: <input type="number" name="mobile_number"></td>
                </tr>
                <tr>
                    <td>Enter your gender</td>
                    <td>
                        <label for="M" class="radio-inline">
                            : <input type="radio" name="gender" id="M" value="Male">Male
                        </label>
                        <label for="F" class="radio-inline">
                            <input type="radio" name="gender" id="F" value="Female">Female
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>Choose your hobbies</td>
                    <td>
                        <label for="cricket" class="form-check form-check-inline">
                            : <input type="checkbox" id="cricket" value="Cricket" name="hobbies[]"> Cricket
                        </label>
                        <label for="foodball" class="form-check form-check-inline">
                            <input type="checkbox" id="foodball" value="Foodball" name="hobbies[]"> Foodball
                        </label>
                        <label for="travelling" class="form-check form-check-inline">
                            <input type="checkbox" id="travelling" value="Travelling" name="hobbies[]"> Travelling
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>Upload your profile image</td>
                    <td>: <input type="file" name="image"></td>
                </tr>
                <tr>
                    <td>Select your DOB</td>
                    <td> <select name="DOB[]">
                            <?php
                            for ($i = 1; $i <= 31; $i++)
                                echo "<option value='$i'>$i</option>";
                            ?>
                        </select>

                        <select name="DOB[]">
                            <?php
                            for ($i = 1; $i <= 12; $i++)
                                echo "<option>$i</option>";
                            ?>
                        </select>

                        <select name="DOB[]">
                            <?php
                            for ($i = 1950; $i <= 2015; $i++)
                                echo "<option>$i</option>";
                            ?>
                        </select>
                    </td>
                </tr>

            </table>
            <hr>
            <button type="submit" class="text-center">Register me!</button>
        </form>
    </main>

</body>
</html>