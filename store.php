<?php

//var_dump($_FILES);

$img_name = $_FILES['image']['name'];
$temp_name = $_FILES['image']['tmp_name'];

move_uploaded_file($temp_name, 'Uploads/'. $img_name);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
    <main class="container py-5">
        <img src="Uploads/<?php echo $img_name?>" width="100" height="100">
        <hr>
        <table border="1">
            <tr>
                <td>Your name</td>
                <td><?php echo $_POST['name']?></td>
            </tr>
            <tr>
                <td>Your email</td>
                <td><?php echo $_POST['email']?></td>
            </tr>
            <tr>
                <td>Your password</td>
                <td><?php echo $_POST['password']?></td>
            </tr>
            <tr>
                <td>Your mobile number</td>
                <td><?php echo $_POST['mobile_number']?></td>
            </tr>
            <tr>
                <td>Your gender</td>
                <td><?php echo $_POST['gender']?></td>
            </tr>
            <tr>
                <td>Your hobbies</td>
                <td>
                    <?php
                    echo implode(",", $_POST['hobbies'])
                    ?>
                </td>
            </tr>
            <tr>
                <td>Your Date of Birth</td>
                <td>
                    <?php
                    echo implode("/", $_POST['DOB'])
                    ?>
                </td>
            </tr>
        </table>
        <hr>
    </main>

</body>
</html>
